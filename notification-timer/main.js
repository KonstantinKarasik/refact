
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function check() {
    var a = getCookie("closed_popup");
    if(a == "true")
    {
        document.getElementById('browserinfo').setAttribute('style', 'display: none;');
    }
}

function setCookie()
{
    document.cookie = "closed_popup=true";
    document.getElementById('warning').setAttribute('style', 'display: none;');
}
function load_popup () {
    var delay_popup = 2000;
    setTimeout("document.getElementById('warning').setAttribute('style', 'margin: 10px auto;')", delay_popup);
}
