<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>Тестовая страница</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
</head>
<body onload="CheckForm(); load()">
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 head-block">

            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 logo">
                    <img src="images/sloi_1.png">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 col-sm-offset-4 col-md-offset-4 col-lg-offset-4
                    contact_icon">
                    <img src="images/contact_icon.png">
                </div>
            </div>

            <form role="form" method="POST" action="handler.php" target="area" name="form">
                <div class="col-sm-10 col-md-4 col-lg-4 col-sm-offset-1 col-md-offset-1 col-lg-offset-1
                    form-group">

                    <label class="name" for="name">Имя<span class="asterisk"> *</span></label>
                    <input type="name" name="name_1" id="name_1" class="form-control" maxlength="30"
                           onblur="CopyName(); CheckForm()" onkeyup="CopyName(); CheckForm()">
                    <input id="name" name="name" type="hidden">

                    <label class="mail" for="mail">E-Mail<span class="asterisk"> *</span></label>
                    <input type="email" name="mail_1" id="mail_1" class="form-control" maxlength="30"
                           id="mail_1" onblur="CopyMail(); CheckMail()" onkeyup="CopyMail(); CheckForm()">
                    <span id="msg">Некорректный e-mail</span>
                    <input id="mail" name="mail" type="hidden">
                </div>

                <div class="col-sm-10 col-md-5 col-lg-5 col-sm-offset-1 col-md-offset-1 col-lg-offset-1
                form-group">
                    <label class="comment" for="comment">Комментарий<span class="asterisk"> *</span></label>
                    <textarea class="form-control" name="comment_1" rows="8" id="comment_1" maxlength="300"
                              onblur="CopyComment(); CheckForm()" onkeyup="CopyComment(); CheckForm()"></textarea>
                    <input id="comment" name="comment" type="hidden">
                </div>

                <div class="col-sm-2 col-md-2 col-lg-2 col-sm-offset-9 col-md-offset-9 col-lg-offset-9
                button-box">
                    <div id="box">Записать</div>
                    <button type="submit" id="button" class="btn btn-success button" onmouseover="CheckForm();
                        CheckMail()" onclick="this.form.reset(); CheckForm()">Записать
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <iframe id="area" name="area" src="handler.php" onload="load()"></iframe>

        <div class="col-sm-12 col-md-12 col-lg-12 main-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4 col-sm-offset-3 col-md-offset-4 col-lg-offset-4 header">
                    Выводим комментарии
                </div>
            </div>
            <div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1
                message-box">
                <div class="row" id="row">
                </div>
            </div>
        </div>
    </div>
    <div>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="scripts.js"></script>
</body>
</html>