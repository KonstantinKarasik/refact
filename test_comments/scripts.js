function CheckMail() {
    var form = document.forms.form;
    var el = document.getElementById("msg");
    var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/igm;
    if (re.test(form.mail_1.value) || form.mail_1.value == "") {
        el.setAttribute("style", "display: none;");

    } else {
        el.setAttribute("style", "display: block;");
        setTimeout(function () {
            el.setAttribute('style', 'display: none;')
        }, 2000);
        document.getElementById("button").setAttribute("style", "display: none;");
        document.getElementById("box").setAttribute("style", "display: block;");
    }
}

function CheckForm() {
    var form = document.forms.form;
    var el = document.getElementById("button");
    var el1 = document.getElementById("box");
    var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
    if (!form.name_1.value || !form.mail_1.value || !form.comment_1.value || !re.test(form.mail_1.value)) {
        el.setAttribute("style", "display: none;");
        el1.setAttribute("style", "display: block;");
    }
    else {
        el.setAttribute("style", "display: block;");
        el1.setAttribute("style", "display: none;");
    }
}

function load() {

    var json = frames[0].document.getElementById('json').innerHTML;
    json = JSON.parse(json);
    document.getElementById("row").innerHTML = '';
    for (i = 0; i < 12; i++) {
        var name = json.name[i];
        var mail = json.mail[i];
        var comment = json.comment[i];
        var id = json.id[i];

        var elem = document.createElement("div");
        elem.className = 'class="col-sm-12 col-md-6 col-lg-4 message-body';
        elem.innerHTML = '<div class="message-top" id="' + id + '"><span>' + name + '</span></div><div ' +
            'class="message-bottom"><p><span class="message-mail">' + mail + '</span></p><br><span ' +
            'class="message-comment">' + comment + '</span></div>';
        document.getElementById("row").appendChild(elem);
        var elems = document.getElementsByClassName('message-top');
        var elems1 = document.getElementsByClassName('message-bottom');
        var elems2 = document.getElementsByClassName('message-mail');
        var elems3 = document.getElementsByClassName('message-comment');
        a = elems[i].getAttribute('id');
        if (a % 2 == 0) {
            elems[i].setAttribute("style", "background-color: #58ad52;");
            elems1[i].setAttribute("style", "background-color: #deebde;");
            elems2[i].setAttribute("style", "color: #58ad52;");
            elems3[i].setAttribute("style", "color: #768275;");
        }
    }
}

function CopyName() {
    document.getElementById('name').value = document.getElementById('name_1').value;
}
function CopyMail() {
    document.getElementById('mail').value = document.getElementById('mail_1').value;
}
function CopyComment() {
    document.getElementById('comment').value = document.getElementById('comment_1').value;
}