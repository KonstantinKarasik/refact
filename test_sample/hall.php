<?php
include 'antet.php';
include 'func.php';
include ''.$hd2.'check_r.php';
include ''.$hd2.'town.php';
include ''.$hd2.'check_c.php';
include ''.$hd2.'buildings.php';
include ''.$hd2.'get_con.php';
include ''.$hd2.'premium.php';
if (isset($_SESSION['user'][0], $_GET['town'])) {
    check_r($_GET['town']);
    check_c($_GET['town'], $_SESSION['user'][10]);
    $town = town($_GET['town']);
    if ($town[1] != $_SESSION['user'][0]) {
        header('Location: login.php');
        die();
    }
    $faction = faction($_SESSION['user'][10]);
    $r = $faction[3];
    $buildings = buildings($_SESSION['user'][10]);
    $c_status = get_con($_GET['town']);
    $data = explode('-', $town[8]);
    $res = explode('-', $town[10]);
    $lim = explode('-', $town[11]);
    $land = explode('/', $town[13]);
    $land[0] = explode('-', $land[0]);
    $land[1] = explode('-', $land[1]);
    $land[2] = explode('-', $land[2]);
    $land[3] = explode('-', $land[3]);
    $out = explode('-', $buildings[7][5]);
    $name = explode('-', $buildings[7][2]);
    $town[8] = explode('-', $town[8]);
    $buildings[11][5] = explode('-', $buildings[11][5]);
    if ($data[7] == 10) {
        $name = $name[1];
    } else {
        $name = $name[0];
    }
} else {
    header('Location: login.php');
    die();
}
$tit = ' - '.$buildings[7][2].'';

$index = 7;

$dp = (100 * ((104 - (4 * ($data[7] + 1))) / 100));
$nalog = ''.$prod[4].'';

$prod = explode('-', $town[9]);
$nalog = ''.$prod[4].'';
if ($_SESSION['user'][10] == 7) {
    $prod[4] = $prod[4] * 2;
}
if ($_SESSION['user'][10] == 8) {
    $row[5] = 0;
}
$lvl = lvl($id);
$prod[4] += 100 * $lvl;
if (premium($_GET['town'], 2)) {
    $prod[0] = ($prod[0] * 1.5);
    $prod[1] = ($prod[1] * 1.5);
    $prod[2] = ($prod[2] * 1.5);
    $prod[3] = ($prod[3] * 1.5);
    $prod[4] = ($prod[4] * 1.5);
}
if (premium($_GET['town'], 3)) {
    $prod[4] = ($prod[4] * 2);
}
$cof = ($town[5] / 100);
if ($cof > 1) {
    $cof = 1;
}

$res0 = floor($res[0]);
$res1 = floor($res[1]);
$res2 = floor($res[2]);
$res3 = floor($res[3]);
$res4 = floor($res[4]);
$prodtt = ($prod[0] - $town[3] - $town[12]);

$json_arr = array('res0' => $res0, 'res1' => $res1, 'res2' => $res2, 'res3' => $res3, 'res4' => $res4,
    'lim0' => $lim[0], 'lim1' => $lim[1], 'lim2' => $lim[2], 'prodtt' => $prodtt, 'prod1' => $prod[1],
    'prod2' => $prod[2], 'prod3' => $prod[3], 'prod4' => $prod[4], 'cof' => $cof);
$json = json_encode($json_arr);
?>


<script src="func.js" type="text/javascript"></script>
<script type="text/javascript">
    el = document.getElementById('json');
    json = el.getAttribute('value');
    json = JSON.parse ( json );

    var res_start0 = json.res0;
    var res_start1 = json.res1;
    var res_start2 = json.res2;
    var res_start3 = json.res3;
    var res_start4 = json.res4;
    var res_limit0 = json.lim0;
    var res_limit1 = json.lim1;
    var res_limit2 = json.lim1;
    var res_limit3 = json.lim1;
    var res_limit4 = json.lim2;
    var res_ph0 = json.prodtt;
    var res_ph1 = json.prod1;
    var res_ph2 = json.prod2;
    var res_ph3 = json.prod3;
    var res_ph4 = json.prod4;
    var res_sec0 = 0;
    var res_sec1 = 0;
    var res_sec2 = 0;
    var res_sec3 = 0;
    var res_sec4 = 0;
    var mor1 = json.cof;
    //-----------------
    function ch_a(v) {
        var land = new Array(4);
        land[0] = "<select class='dropdown' name='b'><?php for ($i = 0; $i < count($land[0]); ++$i) {
            if ($land[0][$i]) {
                echo "<option value='".$i."'>Ферма №".($i + 1).' ['.$land[0][$i].']</option>';
            }
        } echo '</select>'; ?>";
        land[1] = "<select class='dropdown' name='b'><?php for ($i = 0; $i < count($land[1]); ++$i) {
            if ($land[1][$i]) {
                echo "<option value='".$i."'>Лесоповал №".($i + 1).' ['.$land[1][$i].']</option>';
            }
        } echo '</select>'; ?>";
        land[2] = "<select class='dropdown' name='b'><?php for ($i = 0; $i < count($land[2]); ++$i) {
            if ($land[2][$i]) {
                echo "<option value='".$i."'>Копальня №".($i + 1).' ['.$land[2][$i].']</option>';
            }
        } echo '</select>'; ?>";
        land[3] = "<select class='dropdown' name='b'><?php for ($i = 0; $i < count($land[3]); ++$i) {
            if ($land[3][$i]) {
                echo "<option value='".$i."'>Шахта №".($i + 1).' ['.$land[3][$i].']</option>';
            }
        } echo '</select>'; ?>";
        land[4] = "<select class='dropdown' name='b'><?php for ($i = 4; $i < count($lang['buildings'][$_SESSION['user'][10] - 1]); ++$i) {
            if ($data[$i]) {
                echo "<option value='".$i."'>".$lang['buildings'][$_SESSION['user'][10] - 1][$i][0].' ['.$data[$i].']</option>';
            }
        } echo '</select>'; ?>";
        document.getElementById("b").innerHTML = land[v];
    }
</script>


<?php
include 'head.php';
include 'inc-b.php';
include 'foot.php';
include 'template/hall_html.php';
include 'template/hall.js';
