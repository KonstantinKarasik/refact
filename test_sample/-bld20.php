<?php
$lvl = (floor(0.1 * $data[20]) + 8);
if ($lvl > 9) {
    $lvl = 9;
}
for ($i = 7; $i < $lvl; ++$i) {
    $dur = explode(':', $units[$i][9]);
    $cost = explode('-', $units[$i][4]);
    $durmin = floor((($dur[0] * 60 + $dur[1]) * ((105 - (5 * $data[20])) / 100)));
    $dursec = (($dur[0] * 60 + $dur[1]) * ((105 - (5 * $data[20])) / 100)) - $durmin;
    $dursec = floor($dursec * 60);
    $durhou = floor($durmin / 60);
    $durmin = $durmin - $durhou * 60;
    $ok = 1;
    $w1 = $w2 = $w3 = $w4 = $r1 = $r2 = $r3 = $r4 = $r5 = 0;
    $trt = str_replace('0', 'e', $units[$i][3]);
    $gg = explode('-', $units[$i][3]);
    for ($b = 0; $b < count($gg); ++$b) {
        if ($gg[$b] == 0) {
            $w1 += 1;
        }
        if ($gg[$b] == 1) {
            $w2 += 1;
        }
        if ($gg[$b] == 2) {
            $w3 += 1;
        }
        if ($gg[$b] == 3) {
            $w4 += 1;
        }
    }
    if (($trt !== 'e') && (!$units[$i][3])) {
        $w1 = $w2 = $w3 = $w4 = 0;
    }
    if ($w1) {
        $w1 = floor($weaps[0] / $w1);
    } else {
        $w1 = 999999;
    }
    if ($w2) {
        $w2 = floor($weaps[1] / $w2);
    } else {
        $w2 = 999999;
    }
    if ($w3) {
        $w3 = floor($weaps[2] / $w3);
    } else {
        $w3 = 999999;
    }
    if ($w4) {
        $w4 = floor($weaps[3] / $w4);
    } else {
        $w4 = 999999;
    }
    if ($cost[0]) {
        $r1 = floor($res[0] / $cost[0]);
    } else {
        $r1 = 999999;
    }
    if ($cost[1]) {
        $r2 = floor($res[1] / $cost[1]);
    } else {
        $r2 = 999999;
    }
    if ($cost[2]) {
        $r3 = floor($res[2] / $cost[2]);
    } else {
        $r3 = 999999;
    }
    if ($cost[3]) {
        $r4 = floor($res[3] / $cost[3]);
    } else {
        $r4 = 999999;
    }
    if ($cost[4]) {
        $r5 = floor($res[4] / $cost[4]);
    } else {
        $r5 = 999999;
    }
    if (($r1 == 0) OR ($r2 == 0) OR ($r3 == 0) OR ($r4 == 0) OR ($r5 == 0) OR ($w1 == 0) OR ($w2 == 0) OR ($w3 == 0)
        OR ($w4 == 0)){
        $ok = 0;
    }
    if ($w1 == 0) {
        $w1 = 999999;
    }
    if ($w2 == 0) {
        $w2 = 999999;
    }
    if ($w3 == 0) {
        $w3 = 999999;
    }
    if ($w4 == 0) {
        $w4 = 999999;
    }
    $cn = min($w1, $w2, $w3, $w4, $r1, $r2, $r3, $r4, $r5);
    if ($cn == 999999) {
        $cn = 0;
    }
    if (!$ok) {
        $cn = 0;
    }
    $data[$i]['persistence'] = ($units[$i][5] + 0.1 * $units[$i][5] * $u_upgrades[$i]);
    $data[$i]['attack'] = ($units[$i][6] + 0.1 * $units[$i][6] * $w_upgrades[$i]);
    $data[$i]['defence'] = ($units[$i][7] + 0.1 * $units[$i][7] * $a_upgrades[$i]);
    $town = $_GET['town'];
    $user = $_SESSION['user'][10];
    $grain = floor($cost[0]);
    $wood = floor($cost[1]);
    $stone = floor($cost[2]);
    $iron = floor($cost[3]);
    $gold = floor($cost[4]);
}

include 'template/-bld20_html.php';
include  'template/-bld20.css';
include 'template/-bld20.js';