
Нужно написать php скрипт, который будет с помощью curl авторизовываться в личном кабинете мтс,
 и отправлять запрос на добавление услуги "переадресация вызова".
 
Для отправки запросов нужно будет использовать curl.

В случае, если будет появляться capcha со стороны mts, 
нужно будет предусмотреть возможность использования сервисов anticaptcha через их api
 
Есть исходники скрипта на python, в котором был подобный функционал
(в файле mts_check.py def set_redirect_mts(login, password, to_number, log):
  """Функция подключается к issa и и устанавливает безусловную переадрессацию на указанный номер""") 
 
https://github.com/varikgit/globalhome_ats/blob/1254c91bd42bd5e1c0aeb8911627fdb6f84334b8/scripts/mts_check.py
